<#
.SYNOPSIS
    Start (and build) the Varying-Vagrant-Vagrants (VVV) environment.

.DESCRIPTION
    The script executes your VVV configuration in vvv-custom.yml.

.INPUTS
    None

.OUTPUTS
    None

.NOTES
    Author : A. Eberhard
    License: MIT

.LINK
    https://gitlab.com/meengit/vvv-helpers

.EXAMPLE
    . start.ps1

    * Run with Windows PowerShell as Administrator
    * Change to script directory before execute. Eg. "cd /path/to/project/win"

    Run with Windows PowerShell ISE:

    * Open script in Windows PowerShell ISE
    * Press the green "Play" button to run the script
#>

$ENV:PATH = "$ENV:PATH;C:\Program Files\Oracle\VirtualBox\;C:\Program Files (x86)\Oracle\VirtualBox\"

$vvvdir = 'vvv-projects'
$scriptdir = $PSScriptRoot
$workdir = "$($scriptdir)\..\..\$($vvvdir)"

Set-Location $workdir

vagrant plugin install vagrant-vbguest
vagrant plugin install vagrant-hostsupdater
vagrant up --provider virtualbox

Set-Location $scriptdir
