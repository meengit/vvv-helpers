<#
.SYNOPSIS
    Validate the minimal dependencies for a Varying-Vagrant-Vagrants (VVV) environment.

.DESCRIPTION
    The script checks, if Vagrant and Virtual Box are installed on a Windows host.

.INPUTS
    None

.OUTPUTS
    Messages, prefixed with [OK!] or [Error], specifies if minimal dependencies are available.

.NOTES
    Author : A. Eberhard
    License: MIT

.LINK
    https://gitlab.com/meengit/vvv-helpers

.EXAMPLE
    . validate.ps1

    * Run with Windows PowerShell as Administrator
    * Change to script directory before execute. Eg. "cd /path/to/project/win"

    Run with Windows PowerShell ISE:

    * Open script in Windows PowerShell ISE
    * Press the green "Play" button to run the script
#>

$ENV:PATH = "$ENV:PATH;C:\Program Files\Oracle\VirtualBox\;C:\Program Files (x86)\Oracle\VirtualBox\"

$toValidate = @('vagrant', 'VBoxManage')
$toInstall = @('https://www.vagrantup.com/downloads.html', 'https://www.virtualbox.org/')

for ($i = 0; $i -lt $toValidate.length; $i++) {
    if (Get-Command $toValidate[$i] -errorAction SilentlyContinue) {
        Write-Host "[OK!] " -foreground green -nonewline
        Write-Host "Dependency $($i+1), " -nonewline
        Write-Host "$($toValidate[$i])'" -foreground blue -nonewline
        Write-Host ", exists."
    }
    else {
        Write-Host "[Error] " -foreground red -nonewline
        Write-Host "Dependency $($i+1), " -nonewline
        Write-Host "'$($toValidate[$i])'" -foreground blue -nonewline
        Write-Host ", does not exist! Please install " -nonewline
        Write-Host "$($toInstall[$i])" -foreground blue
    }
}
