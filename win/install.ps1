<#
.SYNOPSIS
    Install Varying-Vagrant-Vagrants (VVV) into 'vvv-projects' directory.

.DESCRIPTION
    Install Varying-Vagrant-Vagrants (VVV) into a 'vvv-projects' directory beside of the 'vvv-helpers' directory.
    An existing 'vvv-projects' directory wouldn't be overwritten. You may delete it before run this script.

.INPUTS
    None

.OUTPUTS
    Messages, prefixed with [Status log] or [INFO log].

.NOTES
    Author : A. Eberhard
    License: MIT

.LINK
    https://gitlab.com/meengit/vvv-helpers

.EXAMPLE
    . install.ps1

    * Run with Windows PowerShell as Administrator
    * Change to script directory before execute. Eg. "cd /path/to/project/win"

    Run with Windows PowerShell ISE:

    * Open script in Windows PowerShell ISE
    * Press the green "Play" button to run the script
#>

$ENV:PATH = "$ENV:PATH;C:\Program Files\Oracle\VirtualBox\;C:\Program Files (x86)\Oracle\VirtualBox\"

$vvvurl = 'https://github.com/Varying-Vagrant-Vagrants/VVV/archive/develop.zip'
$vvvname = 'VVV-develop'
$vvvout = 'tmp'
$vvvdir = 'vvv-projects'

$scriptdir = $PSScriptRoot
$workdir = "$($scriptdir)\..\.."

Set-Location -Path $workdir

Write-Host "[Status log] Downloading VVV project files"

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Invoke-WebRequest -Uri $vvvurl -OutFile "$($vvvout).zip"

Expand-Archive "$($vvvout).zip" -Force

Write-Host "[Status log] Creating directory 'vvv-projects' in $(Get-Location)"
Write-Host "[INFO log] If the directory 'vvv-projects' already exists, nothing will be copied or overwritten. You may delete it before."

try {
        Move-Item "$($vvvout)\$vvvname" -Destination $workdir -Force -ErrorAction SilentlyContinue
        Rename-Item -Path $vvvname -NewName $vvvdir -ErrorAction SilentlyContinue
        Copy-Item -Path "$($workdir)\$($vvvdir)\vvv-config.yml" -Destination "$($workdir)\$($vvvdir)\vvv-custom.yml" -Force -ErrorAction SilentlyContinue
        Remove-Item "$($workdir)\$($vvvname)" -Force -Recurse -ErrorAction SilentlyContinue
    }

catch {
    # No catch needed because of SilentlyContinue
}

Write-Host "[Status log] Cleaning up temporary files"

Remove-Item "$($workdir)\$($vvvout)" -Force -Recurse
Remove-Item "$($workdir)\$($vvvout).zip" -Force

Set-Location $scriptdir
